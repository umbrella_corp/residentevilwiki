<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <title>Modificar Videojuegos | ResidentEvilWiki</title>
        <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet"> 
        <style>
            body{
                font-family: 'Press Start 2P', cursive;
                background-color: lightgrey;
                padding-left: 20px;
                padding-right: 20px;
            }
            #EstiloTabla  {

                border-collapse: collapse;
                width: 100%;
            }

           .navbar {
                overflow: hidden;
                background-color: #333;

            }

            .navbar a {
                float: left;
                font-size: 16px;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            .navbar a:hover, .dropdown:hover .dropbtn, .dropbtn:focus {
                background-color: darkred;
            }

            .encabezado{
                background-size: auto;
                height: 375px;
                background-image:url("https://cdn.wallpapersafari.com/23/32/HaFtxW.jpg");
            }

            input[type=text], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                font-family: 'Press Start 2P', cursive;

            }

            input[type=submit] {
               width: 100%;
                background-color: darkred;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
                font-family: 'Press Start 2P', cursive;
            }

            input[type=submit]:hover {
                background-color: darkred;
            }
        </style>
    </head>
    <body>
        <div class="encabezado" >
        </div>
        <div class="navbar">
            <a href="index.htm">Inicio</a>
            <a href="anadir_Videojuego.htm">Añadir videojuego</a>
               <a href="buscar_Videojuego.htm?id=1" >Buscar</a>
               <a style="float:right" href="login.htm" >Salir</a>
        </div>
        <div class="panel  panel-info">
            <div class="panel-heading"><h1 style="padding-top: 50px">Modificar videojuego</h1></div>
            <form method="POST">
                <label>TITULO: </label>
                <input type="text" name="titulo">
                <label>AÑO: </label>
                <input type="text" name="ano">
                <label>DESARROLLADOR: </label>
                <input type="text" name="desarrollador">
                <label>PLATAFORMA:  </label>
                <input type="text" name="plataforma">
                <label>SERIE:  </label>
                <input type="text" name="serie">
                <input type="submit" value="MODIFICAR VIDEOJUEGO">
            </form>

        </div>
    </body>
</html>
