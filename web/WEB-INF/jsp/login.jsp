<%-- 
    Document   : login
    Created on : 01-may-2019, 17:12:30
    Author     : Carlos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
        <style>
            body{
                font-family: 'Press Start 2P', cursive;
                background-color: lightgrey;
                padding-left: 20px;
                padding-right: 20px;
            }
            input[type=password], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                font-family: 'Press Start 2P', cursive;

            }

            input[type=text], input[type=password] {
                width: 100%;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            button {
                width: 100%;
                background-color: darkred;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
                font-family: 'Press Start 2P', cursive;
            }

            button:hover {
                opacity: 0.8;
            }

            .imgcontainer {
                text-align: center;
                margin: 24px 0 12px 0;
            }

            img.avatar {
                width: 30%;
                border-radius: 50%;
            }


            span.psw {
                float: right;
                padding-top: 16px;
            }


            input[type=text], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                font-family: 'Press Start 2P', cursive;

            }

            input[type=submit] {
                width: 100%;
                background-color: darkred;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border-radius: 4px;
                cursor: pointer;
                font-family: 'Press Start 2P', cursive;
                box-sizing: border-box;
                border: 1px solid #ccc;
            }

            input[type=submit]:hover {
                background-color: darkred;
            }

            a {
                background-color: darkred;
                color: white;
                padding: 14px 25px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
            }


        </style>
    </head>
    <body>
        <form method="POST">
            <div class="imgcontainer">
                <img src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2e0096eb-0369-4a6e-8daf-4d9cef9ee5ae/d2zs403-b635b26b-b423-4020-a471-a7016e14ae6d.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzJlMDA5NmViLTAzNjktNGE2ZS04ZGFmLTRkOWNlZjllZTVhZVwvZDJ6czQwMy1iNjM1YjI2Yi1iNDIzLTQwMjAtYTQ3MS1hNzAxNmUxNGFlNmQucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.lG70qhV7JTFxOd-gCTW_Q2Fs0qQ0gzoVjGo5s14XXG0" alt="Avatar" class="avatar">
            </div>
            <p>Bienvenido a la corporación Umbrella. Para acceder a nuestros datos debes formar parte de nuestro equipo.
                Si quieres pasar a ser uno de nuestros empleados, por favor, clica en el botón de registro y crea un nuevo usuario.</p>
            <div >
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" required>
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" required>
                <button type="submit" value="">Login</button>
                <a style=" border: none; border-radius: 4px;" href="altaUsuario.htm">Registrarse</a>
            </div>
        </form> 

    </body>
</html>
