<%-- 
    Document   : errorLog
    Created on : 01-may-2019, 22:09:49
    Author     : Carlos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet"> 
        <style>
            body{
                font-family: 'Press Start 2P', cursive;

                padding-left: 20px;
                padding-right: 20px;
                background-image: url("https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/video-thumbnails/dailymotion_k3STumQNdfWGk6enTib.jpg");
                background-repeat: no-repeat;
                background-size: cover;

            }
            input[type=password], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                font-family: 'Press Start 2P', cursive;

            }

            input[type=text], input[type=password] {
                width: 100%;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            button {
                width: 100%;
                background-color: darkred;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
                font-family: 'Press Start 2P', cursive;
            }

            button:hover {
                opacity: 0.8;
            }

            .imgcontainer {
                text-align: center;
                margin: 24px 0 12px 0;
            }

            img.avatar {
                width: 30%;
                border-radius: 50%;
            }


            span.psw {
                float: right;
                padding-top: 16px;
            }


            input[type=text], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                font-family: 'Press Start 2P', cursive;

            }

            input[type=submit] {
                width: 100%;
                background-color: darkred;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border-radius: 4px;
                cursor: pointer;
                font-family: 'Press Start 2P', cursive;
                box-sizing: border-box;
                border: 1px solid #ccc;
            }

            input[type=submit]:hover {
                background-color: darkred;
            }

            a {
                background-color: darkred;
                color: white;
                padding: 14px 25px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
            }
        </style>
    </head>
    <body>
        <div>
            <h1 style="color: darkred">¡¡¡Login Incorrecto!!!</h1>
            <p style="color: darkred">Aun no eres un usuario de Umbrella Corporation?</p>
            <a  style=" border: none; border-radius: 4px;" href="altaUsuario.htm">¡Hazte miembro ahora!</a>
        </div>
    </body>
</html>
