<%-- 
    Document   : altaUsuario
    Created on : 01-may-2019, 23:03:44
    Author     : Carlos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
      <style>
            body{
                font-family: 'Press Start 2P', cursive;
                background-color: lightgrey;
                padding-left: 20px;
                padding-right: 20px;
            }
            input[type=password], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                font-family: 'Press Start 2P', cursive;

            }

            input[type=text], input[type=password] {
                width: 100%;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            button {
                width: 100%;
                background-color: darkred;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
                font-family: 'Press Start 2P', cursive;
            }

            button:hover {
                opacity: 0.8;
            }

            .imgcontainer {
                text-align: center;
                margin: 24px 0 12px 0;
            }

            img.avatar {
                width: 30%;
                border-radius: 50%;
            }


            span.psw {
                float: right;
                padding-top: 16px;
            }


            input[type=text], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                font-family: 'Press Start 2P', cursive;

            }

            input[type=submit] {
                width: 100%;
                background-color: darkred;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border-radius: 4px;
                cursor: pointer;
                font-family: 'Press Start 2P', cursive;
                box-sizing: border-box;
                border: 1px solid #ccc;
            }

            input[type=submit]:hover {
                background-color: darkred;
            }

            a {
                background-color: darkred;
                color: white;
                padding: 14px 25px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
            }
        </style>
    </head>
    <body>
        <form method="POST">
            <div class="imgcontainer">
                <img src="https://i.imgur.com/xjPLUVw.gif" alt="Avatar" class="avatar">
            </div>
            <div>
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="username" required>
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="pass" required>
                <button type="submit" value="">Crear usuario</button>
            </div>
        </form>
    </body>
</html>
