<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <title>Listar Videojuegos | ResidentEvilWiki</title>
        <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet"> 
        <style>
            body{
                font-family: 'Press Start 2P', cursive;
                background-color: lightgrey;
                padding-left: 20px;
                padding-right: 20px;
            }
            a {
                color: red
            }
            #EstiloTabla  {

                border-collapse: collapse;
                width: 100%;
            }

            #EstiloTabla  td, #customers th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #EstiloTabla tr:nth-child(even){background-color: #f2f2f2;}

            #EstiloTabla  tr:hover {background-color: #ddd;}

            #EstiloTabla  th {
                padding-top: 12px;
                padding-bottom: 12px;
                background-color: black;
                color: white;
                text-align: center;
            }
            .navbar {
                overflow: hidden;
                background-color: #333;

            }

            .navbar a {
                float: left;
                font-size: 16px;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            .navbar a:hover, .dropdown:hover .dropbtn, .dropbtn:focus {
                background-color: darkred;
            }

            .encabezado{
                background-size: auto;
                height: 375px;
                background-image:url("https://cdn.wallpapersafari.com/23/32/HaFtxW.jpg");
            }

        </style>
    </head>
    <body>
        <div class="encabezado" >
        </div>
        <div class="navbar">
            <a style="align-content: " href="index.htm">Inicio</a>
            <a href="anadir_Videojuego.htm">Añadir videojuego</a>
             <a href="buscar_Videojuego.htm?id=1" >Buscar</a>
            <a style="float:right" href="login.htm" >Salir</a>
        </div>
     
        <div class="panel  panel-info">
            <div class="panel-heading"><h1 style="padding-top: 50px">Juegos disponibles</h1></div>
            <table id="EstiloTabla">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>TITULO</th>
                        <th>AÑO</th>
                        <th>DESARROLLADOR</th>
                        <th>PLATAFORMAS</th>
                        <th>SERIE</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    <!--Creo un ForEach para recorer la lista y así pintar los juegos obtenidos con el método del controlador mediante el atributo items-->
                    <c:forEach var="juego" items="${listaVideojuegos}">
                        <tr >
                            <td align="center">${juego.id}</td>
                            <td align="center">${juego.titulo}</td>
                            <td align="center">${juego.ano}</td>
                            <td align="center">${juego.desarrollador}</td>
                            <td align="center">${juego.plataforma}</td>
                            <td align="center">${juego.serie}</td>
                            <td align="center"><a href="modificar_Videojuego.htm?id=${juego.id}" >Modificar</a>
                                <a href="eliminar_Videojuego.htm?id=${juego.id}" >Eliminar</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
