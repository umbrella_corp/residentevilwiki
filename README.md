
# ¡Bienvenido al ResidentEvilWiki Project!

Este proyecto hemos intentado inspirarlo en *Resident Evil*  por ello se nos ocurrió la idea de crear un **proyecto web** implementando el **Framework de Spring Web MVC** que permite a los más fans del videojuego listar, buscar, insertar, modificar, eliminar y exportar en JSON y XML sobre este.


# Estructura del proyecto
Este software encomendado por ***@teacherdev5*** sigue unas pautas genéricas que encontramos en un [documento PDF](https://www.vidalibarraquer.net/moodle/pluginfile.php/63689/mod_assign/introattachment/0/Projecte.%20Servei%20REST%20amb%20Spring.pdf?forcedownload=1) en un lugar con alta contaminación del virus llamado **Moodle**. 

## Paquetes del proyecto

**Como paquetes en este proyecto tenemos:**

### Configuration
En este paquete podremos encontrar la configuración de conexión con los servidores de ***Umbrella Corp*** y que realizará la conexiones con este.

### Modelo
En el paquete de los modelos podremos encontrar los modelo de **Videojuegos y User** ya que son las 2 tablas que tenemos en nuestro **servidor MySQL.**

### Controlador
En el paquete controlador encontraremos el controlador que **permitirá hacer todas las acciones del CRUD y la gestión de logueo de usuarios.**

## Web Pages

### JSP y otros archivos
En esta carpeta obtendremos las vistas y el diseño de la Wiki. Cada archivo lleva su estilo **CSS con fuentes de Google Fonts,** colores oscuros predominando el **rojo, negro y gris.  En total contamos con 7 JSP** las cuales cada una llevarán a una web vista por el usuario. También contamos con el archivo XML *dispatcher-servlet* que es uno de los archivos más importantes del proyecto ya que **enlaza las vistas con el controlador.**

#### Web Login
La web de login se compone del **logo de Umbrella Corp**. con **una breve explicación seguidos por 2 inputs** para introducir el usuario y la contrseña. También cuenta con **2 botones uno para autenticarse y otro para registrarse** en el caso de que no tenga usuario. 

![WebLogInSuccess](images/ReadMe/WebLogInSuccess.gif)

Si al autenticarse el usuario **se a equivocado le llevará a una web que le dirá al usuario** que se a equivocado o que su cuenta no existe y **le sugerirá que crearse una.**

![WebLogInError](images/ReadMe/WebLogInError.gif)

#### Web Registro

La web de registro es similar a la de Login **permitiendo registrar al usuario** y **guardarlo en la base de datos.**

![WebRegistrar](images/ReadMe/WebRegistrar.gif)

#### Web Inicio

En la pagina principal **listaremos todos los videojuegos de la base de datos**, **cada videojuego tiene su propio "*Modificar*" y "*Eliminar*"** para hacer estas funciones sobre el mismo. También cuenta con con una **barra de navegación que nos permitirá hacer más acciones del CRUD** como añadir un videojuego o buscarlo entre otras cosas.

![WebInicio](images/ReadMe/WebInicio.PNG)

#### Web Añadir videojuegos

En la barra de navegación como hemos comentado antes, se nos abrirá la web para añadir un videojuego. El cual **la ID va siendo generada automáticamente** el resto de opciones **son inputs que al final del formulario cuentan con un botón para insertarlo en la base de datos**.
![WebInsertar](images/ReadMe/WebInsertar.gif)

#### Web Modificar videojuegos
La web de modificar **modificará el videojuego seleccionado de la tabla** en la web principal o en el de búsqueda. **La interfaz y opciones será similar al de añadir los videojuegos.**
![WebModificar](images/ReadMe/WebModificar.gif)

#### Web Eliminar videojuegos

Por último, **el videojuego seleccionado de la tabla** en la web principal o en el de búsqueda **se eliminará de a base de datos**.
![WebEliminar](images/ReadMe/WebEliminar.gif)

# Organización y repartición de tareas.

|Asignada a| Tareas |
|--|--|
| **David (@XRuppy)** | Creación y preparación de la máquina virtual (Servidor MySQL), preparación del proyecto, creación de las vistas y código en general relacionadas con el CRUD.
| **Carlos (@39913699m)** | Creación de las vistas y código en general relacionadas con el logueo y registro de usuarios.|
| **Isaac (@39920459a)**| Creación de métodos en el controlador relacionados con la exportación de listas de videojuegos en JSON y XML. |

# Herramientas utilizadas

 - NetBeans 8.2
 - GitLab
 - Stackedit
 - Google Fonts
 - W3Schools
 - Firefox 66.0.3
 - API ModelAndView y JdbcTemplate

# Componentes del grupo

|Nombre/NickID| Correo Electrónico | 
|--|--|
| David García Donaire / @XRuppy | 48021058w@vidalibarraquer.net |
| Carlos Solís Caño / @39913699m| 39913699m@vidalibarraquer.net |
| Isaac Martínez Moreno / @39920459a| 39920459a@vidalibarraquer.net |

