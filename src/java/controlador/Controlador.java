/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Configuration.Conexion;
import Modelo.User;
import Modelo.Videojuegos;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author David, Carlos, Isaac
 */
@Controller
public class Controlador {

    /**
     * Instanciamos la conexión
     */
    Conexion conexion = new Conexion();
    /**
     * Instanciamos el JdbcTemplate y le pasamos por parámetro la conexión
     */
    JdbcTemplate jdbcTemplate = new JdbcTemplate(conexion.Conectar());
    ModelAndView modelAndView = new ModelAndView();
    long id;
    List datos;

    @RequestMapping(value = "buscar_Videojuego.htm", method = RequestMethod.GET)
    public ModelAndView BuscarVideojuego(HttpServletRequest request) {
        id = Long.parseLong(request.getParameter("id"));
        String query = "SELECT * FROM videojuegos WHERE id=" + id;
        datos = this.jdbcTemplate.queryForList(query);
        modelAndView.addObject("listaVideojuegos", datos);
        modelAndView.setViewName("buscar_Videojuego");
        return modelAndView;
    }

    /**
     * Dentro de este método introduciremos por String la query. Seguido con un
     * método del JdbcTemplate obtenemos los datos en lista que los agrega a los
     * atributos del modelo (Videojuegos). Por último devuelve el nombre de la
     * vista que debe resolver DispatcherServlet para que se abra la página.
     */
    @RequestMapping("index.htm")
    public ModelAndView ListarTodosVideojuegos() {
        String query = "SELECT * FROM videojuegos";
        datos = this.jdbcTemplate.queryForList(query);
        modelAndView.addObject("listaVideojuegos", datos);
        modelAndView.setViewName("index");
        return modelAndView;
    }

    /**
     * Metodo para abrir la pagina de logeo al inicio de la aplicacion
     */
    @RequestMapping(value = "login.htm", method = RequestMethod.GET)
    public ModelAndView login() {
        modelAndView.setViewName("login");
        return modelAndView;
    }

    /**
     * Metodo para abrir la pagina de altaUsuario. Es la pagina encargada
     * de crear un usuario nuevo en la base de datos
     */
    @RequestMapping(value = "altaUsuario.htm", method = RequestMethod.GET)
    public ModelAndView altaUsuario() {
        modelAndView.addObject(new User());
        modelAndView.setViewName("altaUsuario");
        return modelAndView;
    }

    /**
     * Metodo para hacer el insert into del usuario escrito.
     * Se recojen los datos del htm mediante el atributo "name" y aqui los insertamos en la
     * sentencia
     */
    @RequestMapping(value = "altaUsuario.htm", method = RequestMethod.POST)
    public ModelAndView altaUsuario(User user) {
        String query = "INSERT INTO users(username, pass) VALUES (?,?)";
        this.jdbcTemplate.update(query, user.getUsername(), user.getPass());
        return new ModelAndView("redirect:/login.htm");
    }

    /**
     * Metodo para comprobar que el usuario ha introducido el logeo correcto
     * recojo el user y el pass con unas variables.
     * Seguidamente entro en la base de datos y recojo la respuesta que me da.
     * Esta se almacena en una lista. Si contiene informacion, quiere decir que la sentencia ha tenido exito y ha conseguido 
     * leer el user o el pass. Entonces, los datos almacenados dentro de la lista los separo mediante varios
     * split, y asi me quedo con el valor deseado y lo almaceno en una variable.
     * Una vez tengo el user y el pass de la base de datos, los comparo para asegurarme de que son
     * los mismos que ha introducido el usuario.
     * Si el logeo es correcto, nos redirecciona a la pagina principal de la web
     */
    @RequestMapping(value = "login.htm", method = RequestMethod.POST)
    public ModelAndView loginn(HttpServletRequest request) {
        String uname = request.getParameter("uname");
        String psw = request.getParameter("psw");
        String logBD = "";
        String pasBD = "";
        String queryLog = "SELECT * FROM users WHERE USERNAME = '" + uname + "'";
        String queryPass = "SELECT * FROM users WHERE PASS = '" + psw + "'";

        List<Map<String, Object>> datos = jdbcTemplate.queryForList(queryLog);
        List<Map<String, Object>> datos2 = jdbcTemplate.queryForList(queryPass);

        //Si la consulta tiene exito... (Si esta vacio la array no apunta correctamente y peta, por eso hago la comprobacion)
        if (datos != null && !datos.isEmpty()) {
            //Spliteando como si no hubiera mañana para sacar el log
            String[] arrayLog = datos.get(0).toString().split(",");
            String[] arrayLog2 = arrayLog[1].split("=");
            String[] arrayLog3 = arrayLog2[1].split("}");
            logBD = arrayLog3[0];
        }

        //Si la segunda consulta tiene exito, hacemos sus respectivos splits como en la primera
        if (datos2 != null && !datos2.isEmpty()) {
            //Spliteando como si no hubiera mañana para sacar el pass
            String[] arrayPas = datos2.get(0).toString().split(",");
            String[] arrayPas2 = arrayPas[2].split("=");
            String[] arrayPas3 = arrayPas2[1].split("}");
            pasBD = arrayPas3[0];
        }

//        System.out.println(logBD);
//        System.out.println(pasBD);
//        System.out.println(uname);
//        System.out.println(psw);
        //Comparo user y pass
        if (uname.equals(logBD) && psw.equals(pasBD)) {
            return new ModelAndView("redirect:/index.htm");
        } else {
            modelAndView.setViewName("errorLog");
            return modelAndView;
        }

    }

    /**
     * Utilizamos este método para abrir la web de añadir videojuego
     */
    @RequestMapping("anadir_Videojuego.htm")
    public ModelAndView anadirVideojuegos() {
        modelAndView.setViewName("anadir_Videojuego");
        return modelAndView;
    }

    /**
     * Utilizamos el método POST para insertar datos en el lado del servidor.
     * Creo la String de la query de insertar en SQL, con el método update de
     * jdbcTemplate introducimos la query y los getters del modelo (Videojuego)
     * y una vez hecha la inserción redirije al indice
     */
    @RequestMapping(value = "anadir_Videojuego.htm", method = RequestMethod.POST)
    public ModelAndView anadirVideojuegos(Videojuegos juego) {
        String query = "INSERT INTO videojuegos(titulo, ano, desarrollador, plataforma, serie) VALUES (?,?,?,?,?)";
        this.jdbcTemplate.update(query, juego.getTitulo(), juego.getAno(), juego.getDesarrollador(), juego.getPlataforma(), juego.getSerie());
        return new ModelAndView("redirect:/index.htm");
    }

    /**
     * Este método con el método que nos ofrece HttpServletRequest el
     * getParameter obtiene el id que arroja la URL y así sabemos que id es la
     * que debe actualizarse mediante la query después nos muestra la web de
     * modificar.
     */
    @RequestMapping("modificar_Videojuego.htm")
    public ModelAndView modificarVideojuegos(HttpServletRequest request) {
        id = Long.parseLong(request.getParameter("id"));
        modelAndView.setViewName("modificar_Videojuego");
        return modelAndView;
    }

    /**
     * Utilizamos el método POST para actulizar datos en el lado del servidor.
     * Creo la String de la query de actualizar en SQL, con el método update de
     * jdbcTemplate introducimos la query y los getters del modelo (Videojuego)
     * y una vez hecha la actulización redirije al indice
     */
    @RequestMapping(value = "modificar_Videojuego.htm", method = RequestMethod.POST)
    public ModelAndView modificarVideojuegos(Videojuegos juego) {
        String query = "UPDATE videojuegos SET titulo=?, ano=?, desarrollador=?, plataforma=?, serie=? WHERE id=" + id;
        this.jdbcTemplate.update(query, juego.getTitulo(), juego.getAno(), juego.getDesarrollador(), juego.getPlataforma(), juego.getSerie());
        return new ModelAndView("redirect:/index.htm");
    }

    /**
     * Este método con el método que nos ofrece HttpServletRequest el
     * getParameter obtiene el id que arroja la URL y así sabemos que id del
     * videjuego que mediante la query eliminará dicho videojuego después nos
     * rediccionará a la web de modificar.
     */
    @RequestMapping("eliminar_Videojuego.htm")
    public ModelAndView eliminarVideojuegos(HttpServletRequest request) {
        id = Long.parseLong(request.getParameter("id"));
        String query = "DELETE FROM videojuegos WHERE id=" + id;
        this.jdbcTemplate.update(query);
        return new ModelAndView("redirect:/index.htm");
    }

    @RequestMapping("exportalJSON_Videojuego.htm")
    public ModelAndView exportalJSON() {
        return new ModelAndView("redirect:/index.htm");
    }
}
