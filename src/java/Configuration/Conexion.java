/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Configuration;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author David
 */
public class Conexion {

    public DriverManagerDataSource Conectar() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://192.168.56.101:3306/residentevilwiki");
        dataSource.setUsername("user");
        dataSource.setPassword("password");
        return dataSource;
    }
}
