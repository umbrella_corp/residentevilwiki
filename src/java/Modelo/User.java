/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Carlos
 */
public class User {
    @Id //Representa a la clave primaria.
    @GeneratedValue(strategy = GenerationType.AUTO) //Auto incrementará su valor
    private String user_id;
    private String username;
    private String pass;

    public User() {
    }

    public User(String user_id, String username, String pass) {
        this.user_id = user_id;
        this.username = username;
        this.pass = pass;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
    
}
