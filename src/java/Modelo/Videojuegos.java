/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
/**
 *
 * @author David
 */
public class Videojuegos {
    private long id;
    private String titulo;
    private String ano;
    private String desarrollador;
    private String plataforma;
    private String serie;

    public Videojuegos() {
    }

    public Videojuegos(long id, String titulo, String ano, String desarrollador, String plataforma, String serie) {
        this.id = id;
        this.titulo = titulo;
        this.ano = ano;
        this.desarrollador = desarrollador;
        this.plataforma = plataforma;
        this.serie = serie;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getDesarrollador() {
        return desarrollador;
    }

    public void setDesarrollador(String desarrollador) {
        this.desarrollador = desarrollador;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }
    
    
}
